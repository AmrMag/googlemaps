//
//  ViewController.swift
//  google maps
//
//  Created by amr on 2/26/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
class ViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, GMSPlacePickerViewControllerDelegate{
   
    

    @IBOutlet weak var mapView: GMSMapView!
     let locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
       locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        mapView.delegate = self
        
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse
        {
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
            locationManager.startUpdatingLocation()
        }
        else if status == .denied
        {
            let alert = UIAlertController(title: "OOPS", message: "Looks like you didn't allow your location , please allow it from settings", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "okay", style: .cancel, handler: { (dissmis) in
                alert.dismiss(animated: true, completion: nil)
            })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    //Get the user location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let location = locations.first {
            //Setup the map camera
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
            
            locationManager.stopUpdatingLocation()
        }
        
    }
    
    
    //Fires when we press the search button
    @IBAction func pickPlace(_ sender: Any) {
        
        //setup the camera for the new view controller "Search Places View controller"
        let center = self.mapView.camera.target
        //Setup the limits for fetching nearby places
        let northEast = CLLocationCoordinate2D(latitude: center.latitude + 0.001, longitude: center.longitude + 0.001)
        let southWest = CLLocationCoordinate2D(latitude: center.latitude - 0.001, longitude: center.longitude - 0.001)
        
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        
        let config = GMSPlacePickerConfig(viewport: viewport)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
    }
    
    
    //Fires when the user selects a place from the list
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        //Get a new camera equal to the coordinates of the new selected place
        let newCamera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 17, bearing: 0, viewingAngle: 0)
        
        //Set the map view camera to the new camera
        self.mapView.camera = newCamera
        
        //Clear all old markers, then add a new marker
        self.mapView.clear()
        let marker = GMSMarker(position: place.coordinate)
        marker.map = self.mapView
        
        
        
    }
    
    
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    

  


}

